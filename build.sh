#!/bin/bash

# TODO convert to JS
cat hexdump.js > all.tmp.js
sed -i '' '/use strict/d' all.tmp.js
sed -i '' '/require/d' all.tmp.js
sed -i '' '/exports/d' all.tmp.js
echo ';(function () {' > all.js
echo "'use strict';" >> all.js
cat all.tmp.js >> all.js
rm all.tmp.js
echo "window.hexdump = hexdump;" >> all.js
echo '}());' >> all.js

mv all.js dist/hexdump.js
uglifyjs dist/hexdump.js > dist/hexdump.min.js
