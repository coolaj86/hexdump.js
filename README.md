# [@root/hexdump](https://git.rootprojects.org/root/hexdump.js)

| Built by [Root](https://rootprojects.org)

Given an `ArrayBuffer`, will create string output similar to the unix `hexdump` command.

For example, the text of "Hello, World!\n" looks something like this:

```txt
        0  1  2  3  4  5  6  7  8  9  A  B  C  D  E  F
0000000 48 65 6c 6c 6f 2c 20 57 6f 72 6c 64 21 0a
000000e
```

# Usage

So far it just does one thing: print an ArrayBuffer in hex, with a header:

## Node.js (and WebPack)

```bash
var hexdump = require('hexdump.js').hexdump;

var str = hexdump(new Uint8Array([ 0, 1, 2, 127, 254, 255 ]));

console.log(str);
```

## Vanilla JS (Browser)

```html
<script src="https://unpkg.com/@root/hexdump/dist/hexdump.js"></script>
```

```html
<script src="https://unpkg.com/@root/hexdump/dist/hexdump.min.js"></script>
```

```javascript
console.log(window.hexdump(new Uint8Array([0, 1, 2, 127, 254, 255])));
```

### CLI

```bash
hexdump.js <filepath>
```

## Install

Centralized:

```bash
# As a library
npm install --save @root/hexdump

# As a global CLI (useful on windows)
npm install --global @root/hexdump
```

## API

```js
hexdump(arrayBuffer, byteOffset, byteLength);
```
