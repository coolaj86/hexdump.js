#!/usr/bin/env node
'use strict';

var hexdump = require('../');
var fsname = process.argv[2];
var fs = require('fs');
var opts = {};

if (!fsname || '--help' === fsname || '-h' === fsname) {
	console.error('Usage: hexdump.js -C <filepath>');
	process.exit(2);
	return;
}

if ('-C' === fsname) {
	opts.C = true;
	fsname = process.argv[3];
}

try {
	fs.statSync(fsname);
} catch (e) {
	console.error(e.message);
	process.exit(3);
	return;
}

var nb = fs.readFileSync(fsname);
var str = hexdump(nb.buffer, nb.byteOffset, nb.byteLength, opts);
console.info(str);
